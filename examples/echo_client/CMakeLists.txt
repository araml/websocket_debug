cmake_minimum_required (VERSION 3.14.8)
project(echo_client)

file (GLOB SOURCE_FILES *.cpp)
file (GLOB HEADER_FILES *.hpp)

include(../../cmake/CMakeHelpers.cmake)

set (Boost_FIND_REQUIRED TRUE)
set (Boost_FIND_QUIETLY TRUE)
set (Boost_DEBUG FALSE)
set (Boost_USE_MULTITHREADED TRUE)
set (Boost_ADDITIONAL_VERSIONS "1.39.0" "1.40.0" "1.41.0" "1.42.0" "1.43.0" "1.44.0" "1.46.1") # todo: someone who knows better spesify these!
set (WEBSOCKETPP_PLATFORM_TLS_LIBS ssl crypto)
set (WEBSOCKETPP_BOOST_LIBS system thread random)

find_package (Boost 1.67.0 COMPONENTS ${WEBSOCKETPP_BOOST_LIBS})

if (Boost_FOUND)
    # Boost is a project wide global dependency.
    include_directories (${Boost_INCLUDE_DIRS})
    link_directories (${Boost_LIBRARY_DIRS})

    # Pretty print status
    message (STATUS "-- Include Directories")
    foreach (include_dir ${Boost_INCLUDE_DIRS})
        message (STATUS "       " ${include_dir})
    endforeach ()
    message (STATUS "-- Library Directories")
    foreach (library_dir ${Boost_LIBRARY_DIRS})
        message (STATUS "       " ${library_dir})
    endforeach ()
    message (STATUS "-- Libraries")
    foreach (boost_lib ${Boost_LIBRARIES})
        message (STATUS "       " ${boost_lib})
    endforeach ()
    message ("")
else ()
    message (FATAL_ERROR "Failed to find required dependency: boost")
endif()

find_package(OpenSSL)
find_package(ZLIB)

init_target (echo_client)

set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++0x")
add_definitions (-ggdb -Og -Wall -Wcast-align) # todo: should we use CMAKE_C_FLAGS for these?

if (WIN32)
    set (WEBSOCKETPP_PLATFORM_LIBS pthread ws2_32 wsock32)
else ()
    set (WEBSOCKETPP_PLATFORM_LIBS pthread rt)
endif ()

add_executable(${TARGET_NAME} ${SOURCE_FILES} ${HEADER_FILES})
target_link_libraries(${TARGET_NAME} ${WEBSOCKETPP_PLATFORM_LIBS})

#build_executable (${TARGET_NAME} ${SOURCE_FILES} ${HEADER_FILES})

link_boost ()
final_target ()

#set_target_properties(${TARGET_NAME} PROPERTIES FOLDER "examples")
